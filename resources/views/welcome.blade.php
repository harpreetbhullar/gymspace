@extends('layouts.master')
@section('content')
    <!-- Content section -->
    <section class="py-5">
        <div class="container">
           <div class="row">
               <div class="col-sm-6">
                   <h1>find your
                       playground</h1>
                   {{--<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
                   <p>Gymspaces is on a mission to give personal trainers access to an unlimited number of indoor spotspaces. Rent where and when it suits you and only pay when you use a space.</p>
                <button class="btn btn-primary">Let's find your playground</button>
               </div>
               <div class="col-sm-6">
                   <img src="{{ asset('images/eaglefit-3959017_960_720.jpg') }}" alt="streching" width="300px" height="400px">
               </div>

           </div>
        </div>
    </section>


    <!-- Content section -->
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                   {{-- <img src="{{ asset('images/eaglefit-3959017_960_720.jpg') }}" alt="streching" width="300px" height="400px">--}}
                </div>

                <div class="col-sm-6">
                    <h1>Gymspaces makes your
                        life easy
                    </h1>
                    {{--<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
                    <p>Gymspaces brings all available indoor workout spaces together in one platform. Access them whenever you need without a gym membership or monthly fees. In other words, we do the hard work, so you can focus on what matters to you.</p>
                    <button class="btn btn-primary">Check available gymspaces</button>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div style="width: 400px; background: black; height: 400px">
                    <h1 style="color: white; text-align: center; vertical-align: center;padding-top: 150px">Why we
                        exist
                    </h1>
                    {{--<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
                    <p style="color: white; text-align: center">...ReadMore</p>
                    {{-- <img src="{{ asset('images/eaglefit-3959017_960_720.jpg') }}" alt="streching" width="300px" height="400px">--}}
                    </div>
                </div>

                <div class="col-sm-6" >

                    <p class="lead">Gymspaces is on a mission to let fitpros access workout spaces where-and whenever they need.</p>
                    <p>
                        We encourage and inspire them to think differently about the possibilities of working out, to take control and adapt to minor and major twists and turns.
                    </p>
                   {{-- <button class="btn btn-primary">Check available gymspaces</button>--}}
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div style="width: 500px; background: black; height: 550px">
                        <h1 style="color: white; text-align: center; padding-top: 100px">Micha Jesenik
                        </h1>
                        <p class="lead" style="text-align: center; color: white">Proffesional Athlete.</p>
                        <p style="text-align: center; color: white;padding: 20px">
                            "
                            Mijn klanten zitten door heel Amsterdam verspreid en het is daarom onhandig voor mij één vaste plek te hebben. Door Gymspaces huur ik een ruimte die gunstig gelegen is voor de klanten trainen we ongeacht het weer.

                        </p>
                        {{-- <img src="{{ asset('images/eaglefit-3959017_960_720.jpg') }}" alt="streching" width="300px" height="400px">--}}
                    </div>
                </div>

                <div class="col-sm-6" >


                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row"> <h1 style="text-align: center; padding-top: 100px">How it
                    works
                </h1></div>
            <div class="row">

                <div class="col-sm-4">


                        <p class="lead" style="text-align: center;"><span style="position: absolute;width: 30px;background: #8a3636; border-radius: 105px; color: white;font-size: 15px;left: 67px">01</span>Create account.</p>
                        <p style="text-align: center; padding: 20px">
                            Create a free account and add your personal details.

                        </p>
                        {{-- <img src="{{ asset('images/eaglefit-3959017_960_720.jpg') }}" alt="streching" width="300px" height="400px">--}}

                </div>

                <div class="col-sm-4" >

                    <p class="lead" style="text-align: center;"><span style="position: absolute;width: 30px;background: #8a3636; border-radius: 105px; color: white;font-size: 15px;left: 67px">02</span>Find and Book.</p>
                    <p style="text-align: center; padding: 20px">
                        Search for the ideal indoor fitness spaces for you or you clients.

                    </p>

                </div>
                <div class="col-sm-4" >
                    <p class="lead" style="text-align: center;"><span style="position: absolute;width: 30px;background: #8a3636; border-radius: 105px; color: white;font-size: 15px;left: 67px">03</span>Go and play</p>
                    <p style="text-align: center; padding: 20px">
                        Geniet van de voordelen van een onbeperkt aantal sportruimtes.

                    </p>

                    <button class="btn btn-primary">Zoek ruimtes</button>

                </div>
            </div>
        </div>
    </section>

@endsection